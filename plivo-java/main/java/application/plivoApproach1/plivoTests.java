package main.java.application.plivoApproach1;

import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by saicharan on 06/02/19.
 */
public class plivoTests extends BaseApi {
  private static final String auth = "Basic TUFPRFVaWVRRMFkyRk1ZSkJMT1c6T0RneVltUXhZVFEyTjJGa05ERmlaVE5oWldZNE1EQXdZV1k0TnpZMA==";
  private static final String BASE_URL = "https://api.plivo.com/v1/Account/MAODUZYTQ0Y2FMYJBLOW";

  plivoTests(){
    super(auth);
  }

  @Test(testName = "accountTest")
  public void accountTest() {
    try {
      String response = getApiResponse(BASE_URL);
      System.out.println(response);
    } catch (IOException e) {
      e.getMessage();
    }
  }

  @Test(testName="MessageTest")
  public void messageTest(){
    try {
    String body =  "{\n  \"src\": \"9175970 23742\",\n  \"dst\": \"919966031085\",\n  \"text\": \"Hello from Plivo!!\"\n}";
    String response = postApiResponse("application/json",String.format("%s/Message",BASE_URL),body);
    System.out.println(response);
  }catch (IOException e){
      e.getMessage();
    }
  }

  @Test(testName="NumberTest")
  public void numberTest(){
    try {
      String response = getApiResponse(String.format("%s/Number",BASE_URL));
      System.out.println(response);
    } catch (IOException e){
      e.getMessage();
    }
  }

  @Test(testName="PricingTest")
  public void pricingTest(){
    try {

      String response = getApiResponse(String.format("%s/Pricing?country_iso=GB",BASE_URL));
      System.out.println(response);
    }catch (IOException e){
      e.getMessage();
    }
  }

  @Test(testName="UUIDTest")
  public void uuidTest(){
    try {
      String response = getApiResponse(String.format("%s/Message/0f467c40-2a0b-11e9-ac27-0651e9348e2e",BASE_URL));
      System.out.println(response);
    }catch (IOException e){
      e.getMessage();
    }
  }

}
