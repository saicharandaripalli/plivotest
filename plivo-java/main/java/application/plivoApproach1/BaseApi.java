package main.java.application.plivoApproach1;

import okhttp3.*;
import org.testng.Assert;

import java.io.IOException;

/**
 * Created by saicharan on 06/02/19.
 */
public class BaseApi {
  private OkHttpClient client;
  private String auth;

  BaseApi() {
    client = new OkHttpClient();
  }

  BaseApi(String auth) {
    client = new OkHttpClient();
    this.auth = auth;
  }

  public String getApiResponse(String url) throws IOException {
    Request request = new Request.Builder()
      .url(url)
      .get()
      .addHeader("Authorization", auth)
      .build();

    Response response = client.newCall(request).execute();
    Assert.assertTrue(response.code() == 200);
    return response.body().string();
  }

  public String postApiResponse(String media, String url, String msgBody) throws IOException {
    MediaType mediaType = MediaType.parse(media);
    RequestBody body = RequestBody.create(mediaType, msgBody);
    Request request = new Request.Builder()
      .url(url)
      .post(body)
      .addHeader("Authorization", auth)
      .build();

    Response response = client.newCall(request).execute();
    Assert.assertTrue(response.code() == 200);
    return response.body().string();
  }

}
