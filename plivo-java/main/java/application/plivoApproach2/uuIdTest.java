package main.java.application.plivoApproach2;

import okhttp3.*;

import java.io.IOException;

/**
 * Created by saicharan on 06/02/19.
 */
public class uuIdTest {
  public static void main(String args[]) throws IOException {
    OkHttpClient client = new OkHttpClient();

    MediaType mediaType = MediaType.parse("application/octet-stream");
    Request request = new Request.Builder()
      .url("https://api.plivo.com/v1/Account/MAODUZYTQ0Y2FMYJBLOW/Message/0f467c40-2a0b-11e9-ac27-0651e9348e2e")
      .get()
      .addHeader("Authorization", "Basic TUFPRFVaWVRRMFkyRk1ZSkJMT1c6T0RneVltUXhZVFEyTjJGa05ERmlaVE5oWldZNE1EQXdZV1k0TnpZMA==")
      .build();

    Response response = client.newCall(request).execute();

    System.out.println(response.body().string());
  }
}
